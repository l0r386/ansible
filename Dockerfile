FROM ubuntu:22.04 as base
WORKDIR /usr/local/bin
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y software-properties-common curl git build-essential ansible sudo && \
    apt-get clean autoclean && \
    apt-get autoremove --yes

FROM base as lore
RUN addgroup --gid 1000 l0r386
RUN adduser --gecos l0r386 --uid 1000 --gid 1000 --disabled-password l0r386
RUN echo "l0r386 ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
# RUN usermod -aG sudo l0r386
USER l0r386
WORKDIR /home/l0r386

FROM lore
USER l0r386
COPY . .
ARG TAGS
ENV TAGS ${TAGS}
CMD ["sh",  "-c",  "ansible-playbook ${TAGS}  local.yml"]

